﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class EnergyBar : MonoBehaviour {

    public static EnergyBar Instance;
    public Text HPText;
    public RectTransform Bar; 
    void Awake()
    {
        Instance = this;
    }
    // Use this for initialization
    void Start () {
	
	}
    public void SetEnergyText(float energy)
    {
        Bar.localScale = new Vector3(energy/5f, 1, 1);
        //HPText.text = "Energy: " + energy;
    }
    // Update is called once per frame
    void Update () {
	
	}
}
