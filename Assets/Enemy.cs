﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
    public static int allEnemysCount = 0;

    public GameObject BulletPrefab;
    Rigidbody2D thisRigidbody2D;
    public Transform Gun;
    float fireDelay;
    public Animator AnimRHand;
    void Awake()
    {
        fireDelay = Time.realtimeSinceStartup + Random.value;
        allEnemysCount += 1;
        thisRigidbody2D = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        AnimRHand.speed = 2;
    }

    float moveTimer = 0.5f;
    void Update()
    {
        if (!AnimRHand.IsInTransition(0))
            AnimRHand.SetInteger("AnimIndex", 0);
        if (Main.Instance != null)
            if (fireDelay + .5f < Time.realtimeSinceStartup&&Vector3.Distance(Main.Instance.transform.position, transform.position)<20&&Main.Instance.HumanForm)
            {
                if (Main.Instance != null)
                {
                    if (Main.Instance.transform.position.x > transform.position.x)
                    {
                        transform.localScale = Vector3.one;
                    }
                    else
                    {
                        transform.localScale = new Vector3(-1, 1, 1);
                    }
                }
                AnimRHand.SetInteger("AnimIndex", 1);
                GameObject bulletObject = Instantiate(BulletPrefab, Gun.position, Quaternion.identity) as GameObject;
                Destroy(bulletObject, 15f);
                fireDelay = Time.realtimeSinceStartup;
            }

        moveTimer -= Time.deltaTime;
        if (moveTimer <= 0 && thisRigidbody2D != null)
        {
            if (Random.value > 0.5f)
            {
                thisRigidbody2D.AddRelativeForce(Vector2.right * 0.5f, ForceMode2D.Impulse);
            }
            else
            {
                thisRigidbody2D.AddRelativeForce(Vector2.left * 0.5f, ForceMode2D.Impulse);
            }
            moveTimer = 0.5f;
        }
        
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Player"&& !coll.gameObject.GetComponent<Main>().HumanForm|| coll.gameObject.tag != "Player"&& coll.gameObject.layer != LayerMask.NameToLayer("Enemy")&&coll.rigidbody!=null)
        {
            if (coll.relativeVelocity.magnitude>4)
            {
                allEnemysCount -= 1;
                Main.OnEnemyKilled();
                Sounds.CreateBlood(transform);
               

                Destroy(gameObject);
                Main.Instance.AddEnergy(1);
            }
        }
    }

}
