﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class UIScript : MonoBehaviour
{
    public static bool isMenuOpened = false;

    public static UIScript instance;
    public GameObject levelClearObject;
    public GameObject buttonObject;
    public RectTransform retryButtonTransform;
    public Text titleLabel;
    public Text scoreText;
    public Text treasuresLabel;

    public Text comboLabel;
    public Text defeatLabel;

    //void OnGUI()
    //{
    //    GUI.Label(new Rect(100, 100, 100, 100), Enemy.allEnemysCount.ToString());
    //}

    void Awake()
    {
        instance = this;
        SceneManager.LoadScene("Level_1");
        OnLevelStarted();
    }

    public static void SetScore(int newScore)
    {
        if (instance != null)
        {
            instance.scoreText.text = newScore.ToString();
        }
        if (Enemy.allEnemysCount == 0)
        {
            instance.levelClearObject.gameObject.SetActive(true);
            instance.scoreText.text = newScore.ToString();
            instance.treasuresLabel.text = Main.Instance.gainedTreasures.ToString();
        }
        OnEnemyKilled();
    }

    public static int comboCount = 0;
    public static float size = 0f;
    public static int timerId = -1;
    public static void OnEnemyKilled()
    {
        comboCount += 1;
        if (comboCount > 1)
        {
            instance.comboLabel.text = "COMBO " + comboCount.ToString();
            instance.comboLabel.gameObject.SetActive(true);

            size = instance.comboLabel.transform.localScale.x;
            if (timerId != -1)
            {
                Timer.Stop(timerId);
            }
            instance.comboLabel.color = Color.red;
            timerId = Timer.Add(0.5f, (animationTime) =>
            {
                float newScale = Mathf.Lerp(size, comboCount * 0.5f, animationTime);
                instance.comboLabel.transform.localScale = new Vector3(newScale, newScale, newScale);
            }, () =>
            {
                timerId = Timer.Add(1f, (colorAnimationTime) =>
                {
                    float alpha = Mathf.Lerp(1f, 0f, colorAnimationTime);
                    instance.comboLabel.color = new Color(1f, 0f, 0f, alpha);
                }, () =>
                {
                    timerId = -1;
                    instance.comboLabel.transform.localScale = Vector3.zero;
                });
            });
        }
    }

    public static void PlayDefeat(System.Action onFinishPlayAction)
    {
        instance.defeatLabel.gameObject.SetActive(true);
        instance.defeatLabel.transform.localScale = Vector3.zero;
        Timer.Add(1f, (animationTime) =>
        {
            float newScale = Mathf.SmoothStep(0f, 1f, animationTime);
            instance.defeatLabel.transform.localScale = new Vector3(newScale, newScale, newScale);
        }, () =>
        {
            Timer.Add(1f, onFinishPlayAction);
        });
    }

    public static void OnLevelStarted()
    {
        isMenuOpened = false;
        instance.levelClearObject.SetActive(false);
        instance.comboLabel.gameObject.SetActive(false);
        instance.comboLabel.transform.localScale = Vector3.zero;
        instance.defeatLabel.gameObject.SetActive(false);

        Sounds.Instance.Music.Play();
    }

    public static void OnLevelComplete(bool isLevelCompleted)
    {
        isMenuOpened = true;
        instance.levelClearObject.SetActive(true);
        instance.treasuresLabel.text = Main.Instance.gainedTreasures.ToString();
        instance.defeatLabel.gameObject.SetActive(false);
        if (isLevelCompleted == true)
        {
            instance.titleLabel.text = "MISSION COMPLETE";
            instance.buttonObject.SetActive(true);
            instance.retryButtonTransform.anchoredPosition = new Vector2(-50, -146f);
        }
        else
        {
            instance.titleLabel.text = "DEFEAT";
            instance.retryButtonTransform.anchoredPosition = new Vector2(130, -146f);
            instance.buttonObject.SetActive(false);
        }
    }

    public void OnRetryButtonClick()
    {
        Main.ReloadCurrentLevel();
    }

    public void OnNextLevelButtonClick()
    {
        Debug.Log("NEED MAKE NEXT LEVEL");
    }
}
