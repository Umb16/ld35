﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Sounds : MonoBehaviour {

    public static Dictionary<string, AudioClip> SoundsDictionary = new Dictionary<string, AudioClip>();
    public static Dictionary<string, GameObject> ParticlesDictionary = new Dictionary<string, GameObject>();
    public AudioSource Source;
    public AudioSource Music;
    public static Sounds Instance;
    // Use this for initialization
    void Awake()
    {
        Instance = this;
        SoundsDictionary = GetSoundsFromPath("Sounds");
        ParticlesDictionary = GetObjectsFromPath("Particles");

    }
    private static Dictionary<string, AudioClip> GetSoundsFromPath(string path)
    {
        Object[] temp = Resources.LoadAll(path);
        Dictionary<string, AudioClip> result = new Dictionary<string, AudioClip>();
        for (int i = 0; i < temp.Length; i++)
        {
            result.Add(((AudioClip)temp[i]).name, (AudioClip)temp[i]);
        }
        return result;
    }
    private static Dictionary<string, GameObject> GetObjectsFromPath(string path)
    {
        Object[] temp = Resources.LoadAll(path);
        Dictionary<string, GameObject> result = new Dictionary<string, GameObject>();
        for (int i = 0; i < temp.Length; i++)
        {
            result.Add(((GameObject)temp[i]).name, (GameObject)temp[i]);
        }
        return result;
    }


    public static void CreateGraveBurst(Transform targetTransform)
    {
        Instantiate(GetParticleSys("SmokeMorph"), targetTransform.position,Quaternion.identity);
    }
    public static void CreateBlood(Transform targetTransform)
    {
        Destroy( Instantiate(GetParticleSys("Blood"), targetTransform.position, Quaternion.identity),3);
    }

    public static void CreateVampreBlood(Transform targetTransform)
    {
        Destroy(Instantiate(GetParticleSys("DieBlood"), targetTransform.position, Quaternion.identity), 20);
    }

    public static void CreateMimiBlood(Transform targetTransform)
    {
        Destroy(Instantiate(GetParticleSys("BloodMini"), targetTransform.position, Quaternion.identity), 3);
    }

    public static void CreateWallBit(Vector2 target)
    {
        Destroy(Instantiate(GetParticleSys("WallBit"), target, Quaternion.identity), 3);
    }
    public static void CreateBalkaBit(Vector2 target)
    {
       // if(Random.value<.2f)
        Destroy(Instantiate(GetParticleSys("BalkaBit"), target, Quaternion.identity), 3);
    }



    public static void PlayEnd()
    {
        Instance.Source.PlayOneShot(GetSound("untitled"));
        Instance.Music.Stop();
    }
    public static void PlayMorph()
    {
        Instance.Source.PlayOneShot(GetSound("DarkSunDead"));
    }
    public static void PlayMorphOff()
    {
        Instance.Source.PlayOneShot(GetSound("teleporter-"),.3f);
    }
    public static void PlayMetal()
    {
        Instance.Source.PlayOneShot(GetSound("Metal"+Random.Range(1,8)),.1f);
    }
    public static void PlayGraveBit()
    {
        Instance.Source.PlayOneShot(GetSound("GraveBit1"), .4f);
    }
    public static void PlayWallBroke()
    {
        Instance.Source.PlayOneShot(GetSound("GraveBit2"), .4f);
        Instance.Source.PlayOneShot(GetSound("BoxFall1"), .1f);
    }

    public static GameObject GetParticleSys(string name)
    {
        GameObject result;
        if (ParticlesDictionary.TryGetValue(name, out result))
            return result;
        else
        {
            return new GameObject();
        }
    }

    
    public static AudioClip GetSound(string name)
    {
        AudioClip result;
        if (SoundsDictionary.TryGetValue(name, out result))
            return result;
        else
        {
            return SoundsDictionary["error"];
        }
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
