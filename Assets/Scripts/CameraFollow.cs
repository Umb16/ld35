﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    public Transform followTransform;
	
	void FixedUpdate ()
    {
        if (followTransform != null)   
        {
            Vector3 pos = followTransform.position;

            pos.z = -10;
            transform.position = pos * 0.5f + transform.position * 0.5f;
        }
	}
}
