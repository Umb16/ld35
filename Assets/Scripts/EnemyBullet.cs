﻿using UnityEngine;
using System.Collections;

public class EnemyBullet : MonoBehaviour
{
    public enum BulletType
    {
        FreeDir,
        LeftOrRightDir
    }
    public BulletType Type;
    public float Speed = 3;
    void Start()
    {
        if (Type == BulletType.FreeDir)
        {
            GetComponent<Rigidbody2D>().velocity = Vector3.Normalize((-transform.position + Main.Instance.transform.position)) * Speed;
        }
        if (Type == BulletType.LeftOrRightDir)
        {
            if (-transform.position.x + Main.Instance.transform.position.x > 0)
                GetComponent<Rigidbody2D>().velocity = Vector3.right * Speed;
            else
                GetComponent<Rigidbody2D>().velocity = Vector3.left * Speed;
        }
        if (transform.position.x - Main.Instance.transform.position.x < 0)
            transform.eulerAngles = new Vector3(0, 0, Vector3.AngleBetween(transform.position, Main.Instance.transform.position));
        else
            transform.eulerAngles = new Vector3(0, 0, Vector3.AngleBetween(transform.position, Main.Instance.transform.position) + 180);
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            if (coll.gameObject.GetComponent<Main>().HumanForm)
            {
                coll.gameObject.GetComponent<Main>().ReceiveDamage();
            }
        }
        Destroy(gameObject);
    }
}
