﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Main : MonoBehaviour
{
    public static int playerScore = 0;
    AudioSource audioSource;
    public float maxHealthPoints = 5f;
    public float healthPoints = 5;
    float energy = 5;
    float energyRegeanDelay;
    public enum AlternativeForm { Rectangle, Triangle, Circle }
    public AlternativeForm currentAlternativeForm = AlternativeForm.Rectangle;
    public static Main Instance;
    float playerSpeed = 0.25f;
    public Rigidbody2D thisRigidbody;
    public BoxCollider2D groundDetector;
    public GameObject boxObject;
    public BoxCollider2D BoxColl;
    public PolygonCollider2D HumanColl;
    public SpriteRenderer HumanRenderer;

    public bool HumanForm = true;
    bool isKeyPressed = false;
    bool isPlayerOnAir = false;

    public GameObject triangleObject;
    public PolygonCollider2D triangleCollider;

    public GameObject circleObject;
    public CircleCollider2D circleCollider;

    public int gainedTreasures = 0;
    public Transform leftWingTransform;
    public Transform rightWingTransform;
    int wingTimer = -1;

    void Awake()
    {
        Instance = this;
    }
    public void AddEnergy(float value)
    {
        energy += value;
        if (energy > 5)
        {
            energy = 5;
        }
        if (energy < 0)
        {
            energy = 0;
        }
        EnergyBar.Instance.SetEnergyText(energy);

    }
    void Start()
    {
        HPBar.Instance.SetHPText(healthPoints);
        EnergyBar.Instance.SetEnergyText(healthPoints);
        FormIndicator.OnFormChanged();
    }
    void SetGrayScreen()
    {
        Camera.main.backgroundColor = Color.gray;
    }
    public void ChangeHealth(int changeValue)
    {
        healthPoints += changeValue;
        HPBar.Instance.SetHPText(healthPoints);
    }

    public void ReceiveDamage()
    {
        healthPoints--;
        Camera.main.backgroundColor = Color.gray * .9f;
        Invoke("SetGrayScreen", .1f);
        HPBar.Instance.SetHPText(healthPoints);
        Vector3 particlesPosition = transform.position;
        if (healthPoints == 0)
        {
            if (wingTimer != -1)
            {
                Timer.Stop(wingTimer);
            }
            Destroy(gameObject);
            Sounds.CreateVampreBlood(transform);
            Sounds.PlayEnd();
            UIScript.PlayDefeat(() =>
            {
                UIScript.OnLevelComplete(false);
                //ReloadCurrentLevel();
            });
        }
        else
            Sounds.CreateMimiBlood(transform);
    }

    public static void ReloadCurrentLevel()
    {
        UIScript.OnLevelStarted();
        Enemy.allEnemysCount = 0;
        playerScore = 0;
        Instance.gainedTreasures = 0;
        SceneManager.LoadScene(0);
    }

    void Update()
    {
        isKeyPressed = false;
        if (HumanForm)
        {
            if (energy < 1)
            {
                AddEnergy(Time.deltaTime * .3f);
            }

            if (!UIScript.isMenuOpened)
            {

                if (Input.GetKey(KeyCode.A))
                {
                    isKeyPressed = true;
                    transform.localScale = new Vector3(-1, 1, 1);
                    if (thisRigidbody.velocity.x > -7f)
                    {
                        thisRigidbody.AddForce(Vector2.left * playerSpeed, ForceMode2D.Impulse);
                    }
                }
                else if (Input.GetKey(KeyCode.D))
                {
                    transform.localScale = new Vector3(1, 1, 1);
                    isKeyPressed = true;
                    if (thisRigidbody.velocity.x < 7f)
                    {
                        thisRigidbody.AddForce(Vector2.right * playerSpeed, ForceMode2D.Impulse);
                    }
                }
                if (Input.GetKeyDown(KeyCode.Space) && !isPlayerOnAir)
                {
                    thisRigidbody.AddForce(Vector2.up * 8.5f, ForceMode2D.Impulse);
                }
            }

            if (!isKeyPressed)
            {
                Vector2 currentVelocity = thisRigidbody.velocity;
                currentVelocity.x *= 0.9f;
                thisRigidbody.velocity = currentVelocity;
            }
        }
        else
        {
            AddEnergy(-Time.deltaTime * .1f);
        }

        if (!UIScript.isMenuOpened)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Polymorph();
            }

            if (Input.GetMouseButtonDown(1))
            {
                ChangeAlternativeForm();
            }
        }

        if (groundDetector.IsTouchingLayers(LayerMask.GetMask("Ground", "FreeForBullet")))
        {
            if (isPlayerOnAir)
            {
                OnGrounded();
            }
            isPlayerOnAir = false;
        }
        else
        {
            if (!isPlayerOnAir)
            {
                OnJump();
            }
            isPlayerOnAir = true;
        }
        //  Debug.Log(thisRigidbody.velocity.x + " " + thisRigidbody.velocity.y);
    }
    void Polymorph()
    {
        if (HumanForm)
        {
            if (energy >= 1)
            {
                Sounds.PlayMorph();
                Sounds.CreateGraveBurst(transform);
                UIScript.comboCount = 0;
                AddEnergy(-1);

                HumanColl.enabled = false;
                thisRigidbody.freezeRotation = false;
                HumanRenderer.enabled = false;
                HumanForm = false;
                //thisRigidbody.AddForce(thisRigidbody.velocity.normalized * 10, ForceMode2D.Impulse);

                ActivateAlternativeObjects();

                Vector3 temp = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                float dashSpeed = 100f;
                if (currentAlternativeForm == AlternativeForm.Triangle)
                {
                    dashSpeed = 1400f;
                }
                else if (currentAlternativeForm == AlternativeForm.Circle)
                {
                    dashSpeed = 50f;
                }

                thisRigidbody.angularVelocity = Input.mousePosition.x < Screen.width / 2 ? dashSpeed : -dashSpeed;
                temp.z = 0;
                thisRigidbody.velocity = Vector3.zero;
                thisRigidbody.AddForce(Vector3.Normalize(temp - transform.position) * 15, ForceMode2D.Impulse);
            }
        }
        else
        {
            Sounds.PlayMorphOff();
            Sounds.CreateGraveBurst(transform);
            energyRegeanDelay = Time.realtimeSinceStartup - 1;
            transform.eulerAngles = Vector3.zero;
            HumanColl.enabled = true;
            thisRigidbody.freezeRotation = true;
            HumanRenderer.enabled = true;
            HumanForm = true;

            boxObject.SetActive(false);
            BoxColl.enabled = false;
            triangleObject.SetActive(false);
            triangleCollider.enabled = false;
            circleObject.SetActive(false);
            circleCollider.enabled = false;
        }
    }

    public void ActivateAlternativeObjects()
    {
        if (currentAlternativeForm == AlternativeForm.Rectangle)
        {
            boxObject.SetActive(true);
            BoxColl.enabled = true;

            triangleObject.SetActive(false);
            triangleCollider.enabled = false;

            circleObject.SetActive(false);
            circleCollider.enabled = false;
        }
        else if (currentAlternativeForm == AlternativeForm.Triangle)
        {
            boxObject.SetActive(false);
            BoxColl.enabled = false;

            triangleObject.SetActive(true);
            triangleCollider.enabled = true;

            circleObject.SetActive(false);
            circleCollider.enabled = false;
        }
        else if (currentAlternativeForm == AlternativeForm.Circle)
        {
            boxObject.SetActive(false);
            BoxColl.enabled = false;

            triangleObject.SetActive(false);
            triangleCollider.enabled = false;

            circleObject.SetActive(true);
            circleCollider.enabled = true;
        }
        InstantHideWings();
    }

    public void ChangeAlternativeForm()
    {
        if (currentAlternativeForm == AlternativeForm.Rectangle)
        {
            currentAlternativeForm = AlternativeForm.Triangle;
        }
        else if (currentAlternativeForm == AlternativeForm.Triangle)
        {
            currentAlternativeForm = AlternativeForm.Circle;
        }
        else
        {
            currentAlternativeForm = AlternativeForm.Rectangle;
        }
        FormIndicator.OnFormChanged();
        if (!HumanForm)
        {
            ActivateAlternativeObjects();
        }

    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (!HumanForm)
        {
            if (coll.gameObject.name.Contains("Balka"))
            {
                Sounds.PlayMetal();
                if (coll.relativeVelocity.magnitude > 9)
                    Sounds.CreateBalkaBit(coll.contacts[0].point);
            }
            if (coll.gameObject.name.Contains("Wall"))
                Sounds.PlayGraveBit();
        }
    }

    public static void OnEnemyKilled()
    {
        playerScore += 10;
        UIScript.SetScore(playerScore);

    }

    public static void ReceiveAward(int count)
    {
        Instance.gainedTreasures += 1;
    }

    public void OnJump()
    {
        if (!HumanForm)
        {
            return;
        }
        if (wingTimer != -1)
        {
            Timer.Stop(wingTimer);
        }
        PlayUpAnimation(() =>
        {
            wingTimer = Timer.Add(0.35f, (animationTime) =>
            {
                float currentAngle = Mathf.Lerp(-16f, 70f, Mathf.Sin(animationTime * Mathf.PI));
                float currentAngleRight = Mathf.Lerp(16f, -70f, Mathf.Sin(animationTime * Mathf.PI));

                leftWingTransform.localEulerAngles = new Vector3(0f, 0f, currentAngle);
                rightWingTransform.localEulerAngles = new Vector3(0f, 0f, currentAngleRight);
            }, () =>
            {
                wingTimer = -1;
            });
        });
    }

    public void PlayUpAnimation(System.Action onFinishAnimation)
    {
        float startScale = leftWingTransform.localScale.x;
        
        leftWingTransform.localEulerAngles = new Vector3(0f, 0f, -16f);
        rightWingTransform.localEulerAngles = new Vector3(0f, 0f, 16f);

        wingTimer = Timer.Add(0.25f, (animationTime) =>
        {
            float currentScale = Mathf.Lerp(startScale, 1f, animationTime);

            leftWingTransform.localScale = new Vector3(currentScale, currentScale, 1f);
            rightWingTransform.localScale = new Vector3(currentScale, currentScale, 1f);
        }, () =>
        {
            wingTimer = -1;
            if (onFinishAnimation != null)
            {
                onFinishAnimation();
            }
        });
    }

    public void OnGrounded()
    {
        if (wingTimer != -1)
        {
            Timer.Stop(wingTimer);
        }
        PlayDownAnimation(null);
    }

    public void PlayDownAnimation(System.Action onFinishAnimation)
    {
        float startScale = leftWingTransform.localScale.x;
        float startAngle = leftWingTransform.localEulerAngles.z;
        float endAngle = 70;

        wingTimer = Timer.Add(0.25f, (animationTime) =>
        {
            float currentAngle = Mathf.LerpAngle(startAngle, endAngle, animationTime);
            float currentScale = Mathf.Lerp(startScale, 0f, animationTime);

            leftWingTransform.localEulerAngles = new Vector3(0f, 0f, currentAngle);
            leftWingTransform.localScale = new Vector3(currentScale, currentScale, 1f);

            rightWingTransform.localEulerAngles = new Vector3(0f, 0f, -currentAngle);
            rightWingTransform.localScale = new Vector3(currentScale, currentScale, 1f);
        }, () =>
        {
            wingTimer = -1;
            if (onFinishAnimation != null)
            {
                onFinishAnimation();
            }
        });
    }

    public void InstantHideWings()
    {
        if (wingTimer != -1)
        {
            Timer.Stop(wingTimer);
        }

        leftWingTransform.localEulerAngles = new Vector3(0f, 0f, -16);
        leftWingTransform.localScale = new Vector3(0f, 0f, 1f);

        rightWingTransform.localScale = new Vector3(0f, 0f, 1f);
        rightWingTransform.localEulerAngles = new Vector3(0f, 0f, 16);
    }
}
