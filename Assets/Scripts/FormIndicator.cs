﻿using UnityEngine;
using System.Collections;

public class FormIndicator : MonoBehaviour
{
    static FormIndicator instance;
    public GameObject rectangleObject;
    public GameObject triangleObject;
    public GameObject circleObject;

    void Awake()
    {
        instance = this;
    }

    public static void OnFormChanged()
    {
        if (Main.Instance.currentAlternativeForm == Main.AlternativeForm.Rectangle)
        {
            instance.rectangleObject.SetActive(true);
            instance.triangleObject.SetActive(false);
            instance.circleObject.SetActive(false);
        }
        else if (Main.Instance.currentAlternativeForm == Main.AlternativeForm.Triangle)
        {
            instance.rectangleObject.SetActive(false);
            instance.triangleObject.SetActive(true);
            instance.circleObject.SetActive(false);
        }
        else
        {
            instance.rectangleObject.SetActive(false);
            instance.triangleObject.SetActive(false);
            instance.circleObject.SetActive(true);
        }
    }
}
