﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HPBar : MonoBehaviour
{
    public static HPBar Instance;
    public Text HPText;
    public RectTransform healthBarTransform;
    public RectTransform redHealthBarTransform;


    void Awake()
    {
        Instance = this;
    }

    int timerId = -1;
    public void SetHPText(float hp)
    {
        healthBarTransform.anchorMax = new Vector2(Main.Instance.healthPoints / Main.Instance.maxHealthPoints, 1f);

        if(timerId != -1)
        {
            Timer.Stop(timerId);
        }
        redHealthBarTransform.anchorMin = new Vector2(Main.Instance.healthPoints / Main.Instance.maxHealthPoints, 0f);
        float lastAnchorMax = redHealthBarTransform.anchorMax.x;
        Timer.Add(1f, (animationTime) =>
        {
            float newAnchorMax = Mathf.SmoothStep(lastAnchorMax, redHealthBarTransform.anchorMin.x, animationTime);
            redHealthBarTransform.anchorMax = new Vector2(newAnchorMax, 1f);
        }, () =>
        {
            timerId = -1;
        }
        );
    }
}
